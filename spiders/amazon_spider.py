import scrapy

class AmazonSpider(scrapy.Spider):
    name = "amazon"

    def start_requests(self):
        urls = [
            'https://www.amazon.co.uk/'
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        page = response.url.split("/")[-2]

        all_urls = set()
        raw_links = response.css("a::attr(href)").extract()
        for link in raw_links:
            all_urls.add(response.urljoin(link))

        with open("output.txt", 'w') as f:
            for url in all_urls:
                f.write(url+"\n")
